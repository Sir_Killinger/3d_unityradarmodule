﻿using UnityEngine;
using System.Collections;

public class FoundObj{

	public Transform parent;
	public Vector3 originOffset;
	public int instanceId;
	public bool touched;

	public bool checkEquals(Transform someObj){
		return (someObj.GetInstanceID() == this.instanceId);
	}

	public FoundObj(Transform newParent){
		//constructor function
		this.parent = newParent;
		this.touched = false;
		this.originOffset = new Vector3 ();
		this.instanceId = newParent.GetInstanceID ();
	}

}
