﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Radar_Controller : MonoBehaviour {

	public string[] tagArray;
	public GameObject[] prefabArray;
	public GameObject[] lineArray;
	public List<FoundObj>[] objHolder;
	public List<GameObject>[] repHolder;
	public List<GameObject>[] lineHolder;

	public Transform radarBlipParent;

	public GameObject matchRotation;
	public Vector3 radarOffset;


	private Vector3 sphereScale;

	public void addGeneric(FoundObj newGeneric, int index){

		Debug.Log ("Adding" + tagArray [index]);
		newGeneric.touched = true;
		objHolder [index].Add (newGeneric);

		//create representative
		GameObject instance = GameObject.Instantiate (prefabArray [index]);
		instance.transform.parent = radarBlipParent;
		instance.transform.localScale = new Vector3 (1, 1, 1);
		repHolder [index].Add (instance);

		//create line
		GameObject lineInstance = (GameObject)GameObject.Instantiate (lineArray [index], new Vector3(), new Quaternion());
		lineInstance.transform.parent = radarBlipParent;
		instance.transform.localScale = new Vector3 (1, 1, 1);
		instance.transform.localPosition = new Vector3 (0, 0, 0);
		instance.transform.localRotation = new Quaternion ();
		lineInstance.transform.parent = radarBlipParent;
		lineHolder [index].Add (lineInstance);


	}

	public void removeGeneric(FoundObj removableGeneric, int index){
		//remove from list
		objHolder [index].Remove (removableGeneric);

		//destroy representative at the end of the list
		List<GameObject> instanceList = repHolder [index];
		GameObject instance = instanceList[0];
		instanceList.Remove (instance);
		GameObject.Destroy (instance);

		//TODO: destroy line at the end of the list
		List<GameObject> lineList = lineHolder [index];
		GameObject theLine = lineList [0];
		lineList.Remove (theLine);
		GameObject.Destroy (theLine);


	}
	
	// Use this for initialization
	void Start () {
		//goodObjs = new List<FoundObj> ();
		//badObjs = new List<FoundObj> ();
		objHolder = new List<FoundObj>[tagArray.Length];
		repHolder = new List<GameObject>[tagArray.Length];
		lineHolder = new List<GameObject>[tagArray.Length];


		sphereScale = radarBlipParent.localScale;

		Debug.Log ("sphere scale is" + sphereScale);

		for(int i=0; i<tagArray.Length; i++){
			objHolder[i] = new List<FoundObj>();
			repHolder[i] = new List<GameObject>();
			lineHolder[i] = new List<GameObject>();
		}

		//Make line objects
	



	}
	
	// Update is called once per frame
	void Update () {
	
		//updateListPositions (goodObjs, goodThingPrefab);

		updateListPositions ();
		removeOutdatedMarkers ();


		fixRadarRotation ();

	}

	void fixRadarRotation(){
		//when we rotate, we want the radar to rotate in the opposite direction
		Vector3 matchTransformAngles = matchRotation.transform.rotation.eulerAngles;
		Quaternion newRotation = matchRotation.transform.rotation;
		transform.localEulerAngles = -newRotation.eulerAngles;

	}

	void removeOutdatedMarkers(){
		//go through each tag array, each obj holder


	}

	void updateListPositions(){

		//go through each tag, update each prefab in the tags list

		for(int i=0; i<tagArray.Length; i++){
			List<FoundObj> theHolder = objHolder[i];
			List<GameObject> theReps = repHolder[i];
			for(int j = 0; j<theHolder.Count; j++){
				//change the list's originOffset property into something relative to the display sphere radar
				//then update a representative to that position

				if(theHolder[j].touched == false){
					removeGeneric (theHolder[j], i);
					break;
				}

				Vector3 finalPosition = theHolder[j].originOffset;
				theHolder[j].touched = false;
				finalPosition += radarOffset;
				theReps[j].transform.localPosition = finalPosition;

				//next draw the line

				//find the two positions
				//draw the line with that material

				Vector3 lPosFirst = theReps[j].transform.localPosition;
				Vector3 lPosSecond = lPosFirst;
				lPosSecond.y = 0;

				drawLine(lPosFirst, lPosSecond, i, j);


			}

		}


	}

	void drawLine(Vector3 first, Vector3 second, int lineIndex, int lineInstanceIndex){

		LineRenderer theLine = (LineRenderer)lineHolder [lineIndex] [lineInstanceIndex].GetComponent<LineRenderer> ();

		theLine.SetPosition (0, first);
		theLine.SetPosition (1, second);
		lineHolder [lineIndex] [lineInstanceIndex].transform.localScale = new Vector3 (1, 1, 1);
		lineHolder [lineIndex] [lineInstanceIndex].transform.localPosition = new Vector3 (0, 0, 0);
		lineHolder [lineIndex] [lineInstanceIndex].transform.localRotation = new Quaternion ();

	}

}
