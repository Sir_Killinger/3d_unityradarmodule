﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class Radar_Broadcaster : MonoBehaviour {

	public Radar_Controller radarController;
	public bool thisEnabled;
	public Transform testSphere;

	private float offsetPercentage;
	private Collider[] justFound;

	// Use this for initialization
	void Start () {

	}
	

	// Update is called once per frame
	void Update () {
		offsetPercentage = (testSphere.lossyScale.x + testSphere.lossyScale.y + testSphere.lossyScale.z) / 3;
		//every frame, check to see if anything is in the circle, send everything in the circle to the radarController

		justFound = Physics.OverlapSphere (testSphere.position, offsetPercentage);
		FoundObj obj;

		int i = 0;
		while (i<justFound.Length) {
			//go through tagLists

			for(int j=0; j<radarController.tagArray.Length; j++){

				if(justFound[i].tag == radarController.tagArray[j]){
					Debug.Log (justFound[i].tag);

					//check to see if this exists in radarController.objHolder
					if(radarController.objHolder[j].Exists( x => x.checkEquals (justFound[i].transform))){
						//obj exists, we already know about it, update it's position

						obj = radarController.objHolder[j].Find ( x => x.checkEquals (justFound[i].transform));

						obj.touched = true;
						//set originOffset
						
						Vector3 newOffset = (obj.parent.position - testSphere.position)/offsetPercentage/2;
						
						obj.originOffset = newOffset;

					}else{
						radarController.addGeneric(new FoundObj(justFound[i].transform), j);
					}

				}
			}

			/*

			if(justFound[i].tag == radarController.goodTag){
				//see if it's in the goodObj list
				if(radarController.goodObjs.Exists ( x => x.checkEquals (justFound[i].transform))){
					//update it's relative position
					obj = radarController.goodObjs.Find ( x => x.checkEquals (justFound[i].transform));

					//set originOffset

					Vector3 newOffset = (obj.parent.position - testSphere.position)/offsetPercentage;

					obj.originOffset = newOffset;

				}else{
					//add it to the list in radarController
					radarController.addGoodGuy(new FoundObj(justFound[i].transform));
				}
			}else if(justFound[i].tag == radarController.badTag){
				if(radarController.badObjs.Exists ( x => x.checkEquals (justFound[i].transform))){
					//update it's relative position

					obj = radarController.badObjs.Find ( x => x.checkEquals (justFound[i].transform));

					//set originOffset

					Vector3 newOffset = (obj.parent.position - testSphere.position)/offsetPercentage;

					obj.originOffset = newOffset;

				}else{
					radarController.addBadGuy(new FoundObj(justFound[i].transform));
					//add it to the list in radarController
				}
			}else{
				//we don't need it, disregard it
			}

			*/
			i++;

		}

	}
}
